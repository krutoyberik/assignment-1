class RioBuilder implements CarBuilder {
	private Car car;
	public RioBuilder(){
		this.car = new Car();
	}
	@Override
	public void buildCompany(){
		car.setCompany("Kia");
	}
	@Override
	public void buildModel(){
		car.setModel("Rio");
	}
	@Override
	public Car getCar(){
		return car;
	}
}

class CeratoBuilder implements CarBuilder {
	private Car car;
	public CeratoBuilder(){
		this.car = new Car();
	}
	@Override
	public void buildCompany(){
		car.setCompany("Kia");
	}
	@Override
	public void buildModel(){
		car.setModel("Cerato");
	}
	@Override
	public Car getCar(){
		return car;
	}
}

class OptimaBuilder implements CarBuilder {
	private Car car;
	public OptimaBuilder(){
		this.car = new Car();
	}
	@Override
	public void buildCompany(){
		car.setCompany("Kia");
	}
	@Override
	public void buildModel(){
		car.setModel("Optima");
	}
	@Override
	public Car getCar(){
		return car;
	}
}