class RoadsterBuilder implements CarBuilder {
	private Car car;
	public RoadsterBuilder(){
		this.car = new Car();
	}
	@Override
	public void buildCompany(){
		car.setCompany("Audi");
	}
	@Override
	public void buildModel(){
		car.setModel("Roadster");
	}
	@Override
	public Car getCar(){
		return car;
	}
}

class CabrioletBuilder implements CarBuilder {
	private Car car;
	public CabrioletBuilder(){
		this.car = new Car();
	}
	@Override
	public void buildCompany(){
		car.setCompany("Audi");
	}
	@Override
	public void buildModel(){
		car.setModel("Cabriolet");
	}
	@Override
	public Car getCar(){
		return car;
	}
}

class CoupeBuilder implements CarBuilder {
	private Car car;
	public CoupeBuilder(){
		this.car = new Car();
	}
	@Override
	public void buildCompany(){
		car.setCompany("Audi");
	}
	@Override
	public void buildModel(){
		car.setModel("Coupe");
	}
	@Override
	public Car getCar(){
		return car;
	}
}