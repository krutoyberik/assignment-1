public abstract class AbstractFactory {
	abstract CarBuilder getCarBuilder(String carType);
}