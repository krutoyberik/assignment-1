class KiaFactory extends AbstractFactory {
	CarBuilder getCarBuilder(String type){
		if (type.equalsIgnoreCase("Rio"))
			return new RioBuilder();
		else if (type.equalsIgnoreCase("Cerato"))
			return new CeratoBuilder();
		else if (type.equalsIgnoreCase("Optima"))
			return new OptimaBuilder();
		return null;
	}
}


class AudiFactory extends AbstractFactory {
	CarBuilder getCarBuilder(String type){
		if (type.equalsIgnoreCase("Roadster"))
			return new RoadsterBuilder();
		else if (type.equalsIgnoreCase("Cabriolet"))
			return new CabrioletBuilder();
		else if (type.equalsIgnoreCase("Coupe"))
			return new CoupeBuilder();
		return null;
	}
}